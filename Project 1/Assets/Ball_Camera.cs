﻿using UnityEngine;
using System.Collections;

public class Ball_Camera : MonoBehaviour 
{

    public GameObject Target;
    Transform TargetPosition;
    Transform Camera;
    Vector3 Offset = new Vector3(0, -2.6f, 3);


    void Start()
    {
        TargetPosition = Target.transform;
        Camera = this.gameObject.transform;
    }

    void LateUpdate()
    {
        Camera.position = TargetPosition.position - Offset;
        
        {

        }
    }
} 