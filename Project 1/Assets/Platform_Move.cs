﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_Move : MonoBehaviour
{

    public bool MoveX;
    public bool MoveY;
    public bool MoveZ;
    public float MoveStepX;
    public float MoveStepY;
    public float MoveStepZ;
    float NapryamX = 1, NapryamY = 1, NapryamZ = 1; // Напрям руху (Вперед-Назад)
    public float OffSetX, OffSetY, OffSetZ = 0; //Зсув платформи
    public float MaxOffSetX, MaxOffSetY, MaxOffSetZ; //Максимальний зсув
    Transform StartPosition; // Поточна прзиція Платформи
    Transform TargetPosition; // Цільова позиція платформи


    void Start()
    {
        StartPosition = this.gameObject.transform;
    }

    void Update()
    {
        if (OffSetX > MaxOffSetX)
            NapryamX = -1;
        if (OffSetX < 0)
            NapryamX = 1;
        if (OffSetX > MaxOffSetX)
            NapryamX = -1;
        if (OffSetY < 0)
            NapryamY = 1;
        if (OffSetX > MaxOffSetX)
            NapryamX = -1;
        if (OffSetZ < 0)
            NapryamZ = 1;
        OffSetX = OffSetX + (MoveStepX * NapryamX);
        OffSetY = OffSetY + (MoveStepY * NapryamY);
        OffSetZ = OffSetZ + (MoveStepZ * NapryamZ);

        transform.position = new Vector3(OffSetX, OffSetY, OffSetZ);
    }
}

