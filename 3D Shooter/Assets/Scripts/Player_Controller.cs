﻿using UnityEngine;

[RequireComponent(typeof(Player_Motor))]
public class Player_Controller : MonoBehaviour
{

    [SerializeField]
    private float speed = 5f;
    private Player_Motor motor;
    private void Start()
    {
        motor = GetComponent<Player_Motor>();
    }
    private void Update()
    {
        float xMov = Input.GetAxisRaw("Horizjntal");
        float zMov = Input.GetAxisRaw("Vertical");

        Vector3 movHor = transform.right * xMov;
        Vector3 movVer = transform.forward * zMov;

        Vector3 velocity = (movHor + movVer).normalized * speed;

        motor.Move (velocity);
    }
} 